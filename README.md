# Port Switcher

THIS (PROBABLY) DOESN'T WORK. DON'T USE IT.

## Intro
This is a simple python script to quickly switch between two servers using uPnP. 
Most of this work isn't my own. This script primarily uses Silas S. Brown's (UPnP router command-line control scripts)[https://ssb22.user.srcf.net/setup/upnp.html] and (Miniupnc)[http://miniupnp.tuxfamily.org/]

Basically, you configure a list of servers and a list of ports. The script asks you which server you want to use, and then switches all of the ports over. 

## Getting started
Run
```pip install -r requirements.txt```

Rename ```example_config.py``` ```config.py```.

Input your port and server information. 
Run ```python port_switcher.py```

All Rights Reserved. Open an issue if you're interested in using, forking, or contributing.
I may also try to add a feature where it sends a request and switches to the backup server if the request fails. (How cool would that be!) 

## Contributing
You're welcome to modify my code to suit your needs. (After all most of it isn't mine anyway). If you'd like to share your ideas: Open an issue or submit a PR. I welcome your ideas and feedback. 
